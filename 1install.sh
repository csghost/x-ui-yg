#!/bin/bash
#
# Encrypted by Rangga Fajar Oktariansyah (Anak Gabut Thea)
#
# This file has been encrypted with BZip2 Shell Exec <https://github.com/FajarKim/bz2-shell>
# The filename '1install.sh' encrypted at Wed Apr 17 05:54:10 UTC 2024
# I try invoking the compressed executable with the original name
# (for programs looking at their name).  We also try to retain the original
# file permissions on the compressed file.  For safety reasons, bzsh will
# not create setuid or setgid shell scripts.
#
# WARNING: the first line of this file must be either : or #!/bin/bash
# The : is required for some old versions of csh.
# On Ultrix, /bin/bash is too buggy, change the first line to: #!/bin/bash5
#
# Don't forget to follow me on <https://github.com/FajarKim>
skip=75

tab='	'
nl='
'
IFS=" $tab$nl"

# Make sure important variables exist if not already defined
# $USER is defined by login(1) which is not always executed (e.g. containers)
# POSIX: https://pubs.opengroup.org/onlinepubs/009695299/utilities/id.html
USER=${USER:-$(id -u -n)}
# $HOME is defined at the time of login, but it could be unset. If it is unset,
# a tilde by itself (~) will not be expanded to the current user's home directory.
# POSIX: https://pubs.opengroup.org/onlinepubs/009696899/basedefs/xbd_chap08.html#tag_08_03
HOME="${HOME:-$(getent passwd $USER 2>/dev/null | cut -d: -f6)}"
# macOS does not have getent, but this works even if $HOME is unset
HOME="${HOME:-$(eval echo ~$USER)}"
umask=`umask`
umask 77

bztmpdir=
trap 'res=$?
  test -n "$bztmpdir" && rm -fr "$bztmpdir"
  (exit $res); exit $res
' 0 1 2 3 5 10 13 15

case $TMPDIR in
  / | */tmp/) test -d "$TMPDIR" && test -w "$TMPDIR" && test -x "$TMPDIR" || TMPDIR=$HOME/.cache/; test -d "$HOME/.cache" && test -w "$HOME/.cache" && test -x "$HOME/.cache" || mkdir "$HOME/.cache";;
  */tmp) TMPDIR=$TMPDIR/; test -d "$TMPDIR" && test -w "$TMPDIR" && test -x "$TMPDIR" || TMPDIR=$HOME/.cache/; test -d "$HOME/.cache" && test -w "$HOME/.cache" && test -x "$HOME/.cache" || mkdir "$HOME/.cache";;
  *:* | *) TMPDIR=$HOME/.cache/; test -d "$HOME/.cache" && test -w "$HOME/.cache" && test -x "$HOME/.cache" || mkdir "$HOME/.cache";;
esac
if type mktemp >/dev/null 2>&1; then
  bztmpdir=`mktemp -d "${TMPDIR}bztmpXXXXXXXXX"`
else
  bztmpdir=${TMPDIR}bztmp$$; mkdir $bztmpdir
fi || { (exit 127); exit 127; }

bztmp=$bztmpdir/$0
case $0 in
-* | */*'
') mkdir -p "$bztmp" && rm -r "$bztmp";;
*/*) bztmp=$bztmpdir/`basename "$0"`;;
esac || { (exit 127); exit 127; }

case `printf 'X\n' | tail -n +1 2>/dev/null` in
X) tail_n=-n;;
*) tail_n=;;
esac
if tail $tail_n +$skip <"$0" | bzip2 -cd > "$bztmp"; then
  umask $umask
  chmod 700 "$bztmp"
  (sleep 5; rm -fr "$bztmpdir") 2>/dev/null &
  "$bztmp" ${1+"$@"}; res=$?
else
  printf >&2 '%s\n' "Cannot decompress ${0##*/}"
  printf >&2 '%s\n' "Report bugs to <fajarrkim@gmail.com>."
  (exit 127); res=127
fi; exit $res
BZh91AY&SY�[�� 6����_���������������������� `2�}7=H^��x}S���W���¶m﫹��v��ٛ'm���n�u����}>��������:���� ^۰ �::(: ���� ʂ�
������<�k4�I��{�  I!h�	�L510G���L#F��ҟ��&�fT=M��!�h4P4��h��  h%LM��`%=�2e=$~�~���M4D6A= x��z��=F542z����@�4a��!#QO�6��ޤ��P��h4   �=G� �   �      MH����Tڞ����Lj~Tz���hi�� a4ɦ�F�4h�h     4  D�)�m ����Fʚ~��?PA�2&�44@ � � �  4�h� A0 	���S�?S��A�SeD��2d��h ���      �v�	���vB@�}-}v���n�y�T"و������|��Y�k��d�gPN�c�U@���x6���>�Q���\����5�#d���S�q�-�n��\��t�R��*�\CK"1OE ���̵�h劲q�����gY�ϱw_h;�t������\�'��)������m�z�
{���F>�gg���h6/hhj���ՆH�T�rJ��N
k�8����L�0�)�hҡ�uI�d <����h��e-o�ݯ�F��$T�/��N^=T�i�
�eP�E2H������ȵ���0��C3Q�MiÁ\��oU[m�0Ҳr��LÆ�c��Y�"��RI!�CSt��,-�N�ћ��ͥ�^,p��6�Av���-���ʭ�3+����0[�B-\Mbf�����F��10�YE�d�]����m��pFx�ɗ�C��r�5��cEc�ڝ����\e�e���W!��Gh?|.S
�	*��B��-���E/Jd���4�@l�� �7)��h`m[����֫X&AM+�eM�Xҥ�(Z�W�q3#]��Gp�A��لK�H��J�b���JRO~��x�5�#��0�>�s5m-JE��}f�e�31sR��|�*���8,�09Lk	�������r�e�A�,��	N�è1�K��znL�9	�t�{�N(]V{��>HO�3Y�����u���֯��2�eF��{UL�k����>� ��C����{ϰ���;X�8�	v�S���@?��3� �����0��ڈ΅�(�l�L�J=X��8?o��M�M�-$���j~' �~*6�kUH�C��#�v���U7�$}�?�'WɎ
r�v����a�4="\��L���.$R�2p�����7����u�,�%a�L*�L��UBj#��b���l�+-��"�)"��Lݙ���Ur6q5�.�h1-W,b%�����*��N���L�d�g�	!	�C�@� �yA�>��KC�d!N����g�Ǭ���s{r���j��O}��L�Z�h�*��,Э&2�3T���j���!�G����։ m�3�#1NA��Rr 3ٶ���Cl��J S$�Q�nRB��[MLa�h��8�{��]��f�pH����m+��j�4�~ݰO��1��&�Ͳ4�reDI;���xH��`���S�&�0�8�8�#$�ESj�O�C�lt@s|:�3�q���]�\tG�F
#���w[����6�|Uv�r@��
�ZHڄ�d>.d6KT��;`�]�\C�M]]e���0��a���c����:�ƙ��I#Ry	eI%
͐���E���;b2e* �.-M36�-𘙚PPN�h�īZ�U2�g	 �Z�gX�~!|�,ڼ�)�q�-QDS�O�H�L���&2�x��Д�����N���R���X�ՙ�2ŵ޺��!U�-��k��X�ג�;�:�t�ő� ������!@D��oϐOaڑ�VQ�j�V-��Ռ�_!�����y�H�q�n�+.Z��[��Wn���a�X-7�7�w��ԧJ��$O�� �@��n:
�L��e�[04\PaN�pA�|��W@�-=7�Îg�Һ�Nѐ4��`�IGGZ�g@&s}�Һy���
���s�W1�ΐ !i�yh��j+'ĂeD�3]92�a%.�d?	�#7�Q��)t�����oQ��Ņ"��G��.Q�˰�c��s6�T�`�1�*���<d���m���/�0�t�%*
S��ɚv
�߆�d�=�pz#�W-e�
!��q+�qF��t�h�'�\��p ���hZ��Ex�Xib�S�[CA�D[Xz������#2��>��5�3��p?1����x{^Kz.�u��&s�JO��A�*Rq�Qrv^z/{��N�`��Z�N�Yyy��|�Kh��=;3��4X!ڄ�B_�C��n/���a���{e'��2E���q�V�����z�!b��aw���J�n��㳰�1�L�
��n8f=7��i��1���ǟ��U?��������R�^�+���,cB^�>��C��)5�MMO�2�8���s;�S~gB�w���G�{,3�m��K��Hh�{�_&S��r�9�[+z��Q��vΐ��!�`�w���x���p��k�>�}2�TM7_��G^�q��B	��,&,��IG��hH拵]E�9SkO7� ���P���Y��V���y���G/����Z.)$ ��QC�Q`3+pv�z�u6R�nj���^���f�_��[���L�F����S$����_��Jè��5z���Mxk�g�`����q܃�Lt�٥�1�� QU�a6���&R���C�M��$A"�j��v�u$6�(.,˸t�%Ue�ej����)S�ґCI�&n��@ƅ��	ĂQB2,��{ڑaV��sD���w��k��y#���\�&�D٥�ME�r[`X�m"��˱���r�t��|7��j�=V�B$DSXn��A�ɉL��)X��D.Qh�Օ���0b�C!�zs�=M���UUUQ��gHR-0K�s����U�I���-�4��$B0�5|gz`�QK@30�ҋ�((-{��	�	�h�������"Py����F�F�U��'��˰��p���{m��P/z?���u�S5����>0{���6}��$1"B>G��l��(��\=Q$3ӦO9p�ǖX��)�%tC�ψ��H	|�Ob�-�v��bc.�2(a�ǂWvY �0>! cP�-�8"����N���tpC����Y�����9��u,)�w�Z���䱨�:B��&?�wYȎ��2r�mٖK2�0�>;�Aݡ��!B��a!}�d��� ������lv� �,*� ֈ���KP �@;b��a 9k���\MU*3!�4¨����OXN����8�N����I�j�}�St[��%Ƨc�K4?��Kx�'�Ha;��s �qD���xe�w��ͫRP*R�+fY�0�\�%�If�Ub�.!a	"A��0(��O[~��:��?g;�v+��+��	��Я�/KA�JQ�"r~C�Ӭ�h@�1��	�Q��Q6�?����j����W���Y�9и�l���Dzٞ���^�� 0�L��Ӑ����p����Q�s�6��4Ȥ;�U��5���P��!�6�.J�{|�5�~�<�a\��ҹPeF�Jf%�$�1�Ϲ��a-�+m�4X�ǈ�'>/��80�n�#�|)�{����uH��eĵ�V-{$�\p-�2�#MZ��&�a�~x;_p����N�[��5���wZ�Q���.b�9�!��;EL�k�f<׷^���n���yy�5\��(ք�B��jH���D>C���Dӣ�-���6��=8����_��CV�kV�Hi���zD�3y(R� <]�AW�̀��B�#k/j�f�n�v2H�ð��麎.~�>D^vC�jw>8��N����q�;���D��'�	���f�~��!���tU	#�EN�j^��*�ݧ���P�lpbb�7-
��3brb#�v����� ��n�U��'�i��]�\Y�ĞI\�3�����&5H���,`�=T�%��e¹�4�Z;����kI�칫�D*X��Zw�t��bU)�X4kg�� ���4�U��6{�S�T�m�J+���ɾ\�px��Y���:]�3V!�#����\J�yٶg�:6��n��%�
&��L����8��ss���.��٣mՕu����&"�SFQ�o�ά�' ��!����\0��}��)�`f�0��:^i���7Ν,���F:܇Y�È���x\�h�AR��9G�"�x�g�fiZ�3An!om�ܼ��
f�m�ƉD�@��_�l��g#N��Z��i�,�o��f1��ʀ�2 \����%6U�9�`�r�I
x0^���2�" 
3�#'v��N((G3�P	7��c��=�k�����FG�k��k0�2C�N�Ǆ�&p��#F�~V�tjI�Pݰ���ZRh��
P�A-��1�ć����/S���L)�g"�y�.�t;��&�
!�'`�
c��8S���:2�q=(��,�Δ"�U@��GXx2�V�q���N2(B"����v�C��"�t��s"H)J���i���#ʝ�ٵ$H)" � �H������o,k�������4�W������x������uE�"�C@r����
�> �n鶱�dl�m�g�p���~Mܠtf4��p0�ϼ��$ꍐᠧ�cd���Q���v3����LzǴP��ZF���{ߪ��H��Z�f��Q����K���5P�{�h���z�`�~҅�t�4�E����i��
�Lb���Y/�����`0�|=v5��b�|m��*el�J�����N�"0^8��"���~��s�4zuQ���,���.��9�	ׄU� �)�I"���p%!�nP�x'ID!��Z��N�v�({b����;�İ>��8�(p���ȑܟ���g�]�PI�_pf|L���a&��
�b�0UQDA@�"HC3�w��7�9B�E�F��쳷~�6��F��6��\����� 錍V�,�� l��]�����{U	�E~6��k�	n|���9�ր��脵������u�1�|C��/��,}�a���e
f:��! !CA�vj�N������ikY��hbR�<~���r?�l��ޠr����i7� k���4�u��Ҡ�˅���q@�3<N��a�!y�e�.��6�朞@4T���r�N|W�jʈ�Ft�rJi"h��X�b�����%��O�X(D��yr�D�	��i�}�Sxs��� �*qGa��z�!\���.b�La�L"-�9�45�Ӵ{Cٰ���'>�N�ߘX��II��p���a�v����-<�yW־6u��`3�vd�� 9��F�F��,�"�x���������Bp�bLD7��C�e
�E��b�0"��YڥҊ
���#� D.6�㎤���'���6:��0�5u��F�Z���,����"0�ѦW����u	@SL��;P���m�&K�G8���I��ΗB�@�)s���9�Uf( C�n���ȹ5�	M�	��P:��r�ױMdV��D4���0m:��9�e�n��[u�:�����#(bPcrQ~�E���ک0)J�MAE��c倾]$���z�X?�K��'��u�����������5�8����bǥ:@r[�qo�ѱ�;S �\���!N��1��ɐ��n�5*nIH<N�\{p� w`���8�'��#��zK�c��F�4��,Wb��	*��Z������D�\�`^L��٬���B���!������V=֙��������S<�ce�L����&{l��)r��u����C�	�V�5�k"�P��5q&�',�l�q)�*P�ؘ�tG���p4!P�%�JH%dJ��d��Т�U��\�Q�)@X���*v�\+sc�!�Zt�̲ ��ϙ�d-���dE;
$W���
WC]9gr�$�K�}'Y���ԩ�މת��ߏ[�k)��Ԓ<mo\\N�V�8Xlŉ�כ�ce��[�K�*Ӽ)8��F�����j��ĠϠ���B��GK���h�J.�}.���[�@��
k1.`xv��y���!�72D�!�;^����L Z$������O5�`>���X�YO������L����Bt {��7���=���ˠ(�}D�T�ue�p2�]G�胑")�]p�Y�Ƌ Q	T�d2�b}��\X���)X�A� ���)��Հ�>�L,0�q���q-�'qM}���nL2��£QSH �Up�(�z��d���w��s��p	`���9��D`��ӳ�`�w����i��w��PxwcZ��Yt5|)�@�y���'PE���,EFBqI)$Y�P�r��%�"n�7;�� 8@�" a��W��H���/-�LĸyC\}�ؖ���8B�:qǴ���>�9LcX� �$Y�M��a���!�y��p�s��&i�!��Ӂh�� @�"4)˨���0(5L��J:�#���Ʋ����\Г���������}��r�hY�4ZԊHA�	7w���=���=)�I HS��>p�vWR2b/��0v�8u�=*Rw�@xc$����<Al���z�R*�z�|�ڞ��YeM�@)�qZ7N�)��1���^��ҠPI�"]C�J�� �	�%S&E{���u�����N9U�"!E��a�,s3�j�`�����m�g{f�W(���T��i�� m��ݳ�q�y���n��P؃��9d�/M�I ;�% ��Bq� ����4�!�!��|��d7���;��#�ΡP	x{Ψ#�nj ǀՎ�i��@�ޙ���iR�Z�D��W��'�|���_G/#@�4��Fk1:ٯ����<��^�cm�7x;�~q��2!榀�HH�:�w6�1��׫ �ө.�̗. �:a�du
��T�2$#�V�EMx��F�mT��"��
T(b�B�$�QZ��%�QX	�K���X��NHK
�_:��ӁI��X�pJL�Pj�G������6�HjN�@�!��k��_�ᒆ��@�bj�%�5��=L�G �O]h@��5���RiM�	vh�C �.���Y���1^#A��ĠW
��h���$�C;�����T�"!�;� ��eM��2;R����uZ�E�^�}�(b@G�`�c,L	�i:X8hә��A��_%̉�*���Rh��c+0$Kq�:�(�K���"��4�]� 0T�ū� Ph'�i�,B!t<�{��+Id^���g��N�R�u�BC�(:Δ5�P���3����+��1ԃ�`�1�h�y��޽`zu�E�)؆A�ĪU{v� �璉a�C�<�2�% �l�Ũ�&��k��b�dӅg�P����\�B6V�R�.N�����lMO��<d��;���a
���8�@�`q�R���"g��2/�#�g�ɸ]�NL!	!�5_p)���������	m�l��8��M��UVI$�n��i6�5���?����נ�p�g��0��e�3��A��������.�<�6�|&#�=�x��`�F��P�����a�/�7��{L)�[[��1�fH'2��a[�f��+�.���#$S`en�د�C�Z#�JA�NT��x�����Xsoٖw9��3m3��dI7����T���u4��*��n0]uv%v����kb���\He���&AP���Ŷ�n�Q��4�s�i��&�.( �$D	zU��W^�f�n��R��&r����,�*��4�5�
o��MK2HC<���XWq���a4q��X���.�-|dh����5�5��bh�i�Y�;WnA{D��NIGQY�ͭ�@�B9I8�=Vx�<�-P:05���C ���Fπ���:bp�M���ȇ4.Z��e��Csq!����Ha����X\1CI((0��8ay:��8�!�Eƨ6=�����n:���L�C��&�	��ή�M@z{@Pӫ���t�
P�37���K�H+B��!�"TN�L]��(����}�G��o�h_0�˸3�dYA�Q:��PQ=zq���$I��^�Ic
�K�t�71Ğ\�elP4��2-4�_6ȡ}�S9bdEE�s
��^�h�<̵c��.�� ��0(v-LWD(�pp
fW�L ���nW������b�X�����{��${1ca"�s��@�|Ⱋ>��na 鶉���oO�A:�fq5���`v)�ܠ��&n�C�:�ًa7r��¦𩂗 n�PA,�T�@�L�� &H���"`�zf��H����O����a��`a|u�T f���5�p^�&b��C#�6<f�	�B��%���Û��v������Z)X��b%�݉��! � �ذ� ! � �gZ� Z��s�NCº�鸃a���ߒ��r�Ud�N��?;��H�V6��8�@�He�b��mS('`I����k�ht�(c�@�0.�4����pm.9 d:���>KZN7TM�9D�Zʁ�K��ު���홖F@��6Q4ݗ��ɰ؁N	��^��?��0dv��vkװ��P|�	�-� �ׁ�
���!�k��h �퉔w��B�D Q�8�s��l��n\�'�N{��"	3�	�SEl��S
> 0/~��@���;�{�Ov�C�o�6���`-"�;
C��x����P���s�_"��GZ����N;��b�a-y,����/,��Dk�ΥJx`Ao:��>N�:��4r+j��0��x�p}�FuK5X%�,Ó��(�"O't�!�8\3^�#:�T���t;ׯ#B�G͔	��(,��X+ �����@u���B=;p1�2	("=���Q��"g�����W�W^�h����zi�څ �K[uYU�uW,>�ܯ(h�?���݅ �L���@c��0{Z���y��&���$���PX@L�3��!!��H�b�c�O|%���l����|��'x|p�)�ɮQ�г`!���L�r2��X��c�(�	ru����D*���������l���F�;�ip"���;�X���,��dZ�}��,kh�1D��p�<���A�.hE��t�V�ĻV/Y��G^�z@֥�}nTw P��4��%r�M ���s?�.@��$Oؗ!y�+S�K�L�CERT?�6]Lom��ǳ����o�e��mys�-/�{�A����6=w.�����r!�"obzv��|�wj� ���Y�js��x�����K�C26��t�9�2�Rȗ@+z��Q {9w�i���w�[=p,7#~�>����g�<wT�Sr.���'�������a�	H�8��7K�*���8x$q
�J0O�F����W��J���
�������J7�2��`}�N�y�@���K��aO�su��!s�\����A��C�Pu��:⭥�ʨ�ObF/#:��D\J%$db�	�.�M�2@�>AdUK:K8Ћ4�IAJAe ��v|񤌢��)��z��C��	�]6���o�\Fݽ	G��]P�3�����/����T$y���<9OI,0�lRAs� �ئE���nC$�����kJh>ڼ�
�b���R��B�������rܽ�k�ao�ʞ�>�ɀ�A��X�G��͐��q��qM���G2zG&ǦwD�vG��5�!]z�Y���ތ�,�Y�a}��"h]�K���������X@H��{Z� 5��b��}3���q��{N�O���*��0�'���@��*��xc�Qf
JF(��"Ja���v���)�.�5{�C<L��B�=T�0�>\�\����
�c��?�Y#��RN�\�|jr}[��W�	��6�e��	]8�\������,����?i´�d7$4�t�5��2�s���W/2�C�+N�Z�q�[��ǢT��O.�Q�wQj1(~��4N��}��>��V���f�C5�+/p��8H��S/�]�B�]4�d���p�/�9B3S�x<���|�^j-(η�#��h�h�`ꚡ�KF�2�2T�^��&>xx,=�=F!�W�b��y��,;V�Ijt���ȞZB���pp�/7\qS��_]p��#]C�z�|Ge��DCw�\�a%�-�︝����✙�4�-i�Ϳ����%�څ���'ALY�ITԶYǈ�(�Ϳh"��I�	��1�#KSOR�g���\�H�$˞a�<�K��*�J�ьXTU����F�-_�6Η����3��\�:���OK@"ߠ���(���.����Zf:ݓ����k���X����Z��uI=S~�jMv���p#�7��Τ���[��If�eA1�%j��Д�/9w�[�T"�	�|��d������C������v+��a��3#���Ԋ����"#�c#3jh��n�`�8�u-��(\���lX�E�G;r���˶nT�B�އ�wtm����ӵ��$�u:����U]J8"� x����X��#������'2��Lr��"�D_�>i�ּ���G1�U�eYG�pu�l�뜛��;��͹& �:w��b�훱��Pff�vs9��_�4F3��c<��T��̮6����|���<�O�ݛ4&��6��������m�v��s�LC��E�c��(��.)�6>c�eUz-�>���nd���#���3�����H�
�r� 